<?php
namespace Beweb\Td\Models;

class Stats {
    private int $pv;
    private int $atk;
    private int $def;

    public function __construct() {
        $this->pv = 0;
        $this->atk = 0;
        $this->def = 0;
    }
}