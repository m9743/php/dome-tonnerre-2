<?php
namespace Beweb\Td\Models;

use Beweb\Td\Models\Interfaces\Fighter;

class Character implements Fighter {
    private Race $race;    
    private Job $job;
    private Stats $stats;

    public function __construct($race, $job) {
        $this->race = $race;
        $this->job = $job;
        $this->stats = new Stats();
        $this->stats->pv = $job->getModifPv();
    }

    public function attack(Fighter $target) {

    }
}