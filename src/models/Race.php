<?php
namespace Beweb\Td\Models;

abstract class Race {
    protected Stats $modifiers;
    
    public function __construct(){
        $this->modifiers = new Stats();
    }

}