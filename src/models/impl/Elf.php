<?php
namespace Beweb\Td\Models\Race\Impl;

use Beweb\Td\Models\Race;

class Elf extends Race {
    public function __construct() {
        $this->modifiers->pv = 75;
        $this->modifiers->att = 5;
        $this->modifiers->def = 15;
    }

    
}