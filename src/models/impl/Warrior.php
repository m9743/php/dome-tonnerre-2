<?php
namespace Beweb\Td\Models\Job\Impl;

use Beweb\Td\Models\Job;

class Warrior extends Job {

    function getModifPv(): int {
        return 150;
    }

    function getModifAtt(): int{
        return 20;
    }
    function getModifDef(): int{
        return 20;
    }

}