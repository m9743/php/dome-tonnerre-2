<?php
namespace Beweb\Td\Models\Race\Impl;

use Beweb\Td\Models\Race;

class Orc extends Race {
    public function __construct() {
        $this->modifiers->pv = 100;
        $this->modifiers->att = 4;
        $this->modifiers->def = 10;
    }
}