<?php
namespace Beweb\Td\Models\Race\Impl;

use Beweb\Td\Models\Race;

class Human extends Race {
    public function __construct() {
        $this->modifiers->pv = 50;
        $this->modifiers->att = 2;
        $this->modifiers->def = 25;
    }
}