<?php
namespace Beweb\Td\Models\Job\Impl;

use Beweb\Td\Models\Job;

class Druid extends Job {
    
    function getModifPv(): int {
        return 100;
    }
    function getModifAtt(): int{
        return 40;
    }
    function getModifDef(): int{
        return 10;
    }
}